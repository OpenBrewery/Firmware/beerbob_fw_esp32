Setup of toolchain on arch:

```
sudo pacman -S python-pyserial python-future python-cryptography
```

```
aur/gcc-xtensa-esp32-elf-bin
```


after cloning repostitory, please run
```
git submodule update --recursive --init
```

```plantuml
@startuml

package "Beerbob FW ESP32" #red {
    [OpenBrewery_BSP_ESP32_submodule] <-- OpenBrewery_BSP_ESP32
    [Beerbob_Application_submodule1] <-- Beerbob_Application
    [ESP32_Glue_logic] <-- OpenBrewery_BSP_ESP32_submodule
    [ESP32_Glue_logic] <-- Beerbob_Application_submodule1
}

package "Beerbob FW Linux" #blue {
    [OpenBrewery_BSP_Linux_submodule] <-- OpenBrewery_BSP_Linux
    [Beerbob_Application_submodule2] <-- Beerbob_Application
    [Linux_Glue_logic] <-- OpenBrewery_BSP_Linux_submodule
    [Linux_Glue_logic] <-- Beerbob_Application_submodule2
}


package  "Beerbob_Application" #green {
    [OpenBreweryFW_lib_submodule1] <-- OpenBreweryFW_lib
    [Application code] <-- OpenBreweryFW_lib_submodule1

}

package "OpenBrewery_BSP_ESP32" #red {
    [ESP_IDF_submodule] <-- ESP_IDF
    [OpenBreweryFW_lib_submodule2] <-- OpenBreweryFW_lib
    [Interfaces implementation1] <-- OpenBreweryFW_lib_submodule2
}

package "OpenBrewery_BSP_Linux" #blue{
    [Boost_in_system] <-- Boost
    [OpenBreweryFW_lib_submodule3] <-- OpenBreweryFW_lib
    [Interfaces implementation2] <-- OpenBreweryFW_lib_submodule3
}

package "OpenBreweryFW_lib" #green {
    [googletest_submodule] <-- googletest
    [ETL_submodule] <-- ETL
    [Platform agnostic code]
    [Platform interfaces]

}

package "ESP_IDF" {

}

package "googletest" {

}

package "ETL" {

}

package "Boost" {

}

legend
Green - Generic x86 compiler
Red - ESP32 xtensa gcc compiler
Blue - Linux compiler

Keep in mind, that if toolchain used for given component is dependent on tolchain used in top level CMakeLists
ie.
If you have launched CMake directly on OpenBreweryFW_lib, default toolchain is used
If you have launched CMake on OpenBrewery_BSP_ESP32, nested OpenBreweryFW_lib is built by ESP32 compiler
end legend



@enduml
```
