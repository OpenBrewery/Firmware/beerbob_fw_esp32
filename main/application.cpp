#include <string.h>
#include <SHT21.h>
#include <HttpServerEsp32.h>
#include <SystemEsp32.h>
#include <lwip/apps/sntp.h>
#include <driver/adc.h>
#include <FirmwareManagerEsp32.h>
#include <etl/callback_timer.h>
#include <esp_sleep.h>
#include <chrono>
//#include <DigitalInputEsp32.h>
//#include <TimerOneShotEtl.h>
//#include <PatternButton.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"

#include "RegistryEsp32.h"
#include "WifiClientEsp32.h"
#include "WifiServerEsp32.h"
#include "Wifi.h"
#include "HttpClientEsp32.h"
#include "I2CInterfaceEsp32.h"
#include "MMA8452.h"
#include "BeerBob.h"

#include "driver/gpio.h"

using namespace std::literals;


static EventGroupHandle_t wifi_event_group;
const int CONNECTED_BIT = BIT0;

static const char *TAG = "ESP";

#define GPIO_LED    (gpio_num_t)23
#define GPIO_OUTPUT_PIN_SEL  (1ULL<<GPIO_LED)
#define GPIO_BUTTON     (gpio_num_t)34

#define ESP_INTR_FLAG_DEFAULT 0

etl::message_timer<64> MessageTimer;
etl::callback_timer<64> CallbackTimer;

Peripheral::WifiClientEsp32 * wifiClient = new Peripheral::WifiClientEsp32(MessageTimer);
Peripheral::WifiServerEsp32 * wifiServer = new Peripheral::WifiServerEsp32(MessageTimer);
Peripheral::Wifi wifi(std::move(*wifiClient),std::move(*wifiServer));

uint8_t recv_buf[16384];
Communication::HttpClientEsp32 httpClient;


etl::string<8192> httpServerResBuffer;
etl::flat_map<etl::string_view,std::function<Communication::HttpResponse(etl::istring&)>,10> GetMap;
etl::flat_map<etl::string_view,std::function<Communication::HttpResponse(etl::istring&, etl::string_view)>,10> PostMap;
Communication::HttpServerEsp32 httpServer(GetMap,PostMap,httpServerResBuffer);


Peripheral::I2CInterfaceEsp32 i2c(I2C_NUM_1,GPIO_NUM_0,GPIO_NUM_4,Peripheral::I2CInterface::Config::Mode::Master,10000);

etl::vector<uint8_t,32> acc_i2c_wbuffer;
etl::vector<uint8_t,32> acc_i2c_rbuffer;
Sensor::MMA8452 accelerometer(i2c,0x1c,acc_i2c_wbuffer,acc_i2c_rbuffer);


etl::vector<uint8_t,32> sht_i2c_wbuffer;
etl::vector<uint8_t,32> sht_i2c_rbuffer;
Sensor::SHT21 sht21(i2c,0x40,sht_i2c_wbuffer,sht_i2c_rbuffer);

System::RegistryEsp32 registry;

System::SystemEsp32 sys(registry);
System::FirmwareManagerEsp32 fw_manager;

etl::string<128> AuthBuffer;

etl::string<8192> httpResBuffer;
etl::string<8192> httpReqBuffer;


//Peripheral::TimerOneShotEtl<std::chrono::milliseconds> ButtonTimer(CallbackTimer);

//etl::flat_map<gpio_num_t,etl::delegate<void()>,10> callbackStorage;


//Peripheral::DigitalInputEsp32 ButtonInput(true,GPIO_BUTTON,Peripheral::DigitalInputEsp32::Pull::PullUp);

BeerBob bob(sys
          , registry
          , fw_manager
//          , ButtonInput
//          , ButtonTimer
          , wifi
          , accelerometer
          , sht21.thermometer
          , sht21.humiditySensor
          , httpClient
          , httpServer
          , httpReqBuffer
          , httpResBuffer
          , AuthBuffer);

Communication::HttpClientEsp32::httpBuf buffer{recv_buf, sizeof(recv_buf)};



static void initialize_nvs()
{
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
        ESP_ERROR_CHECK( nvs_flash_erase() );
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);
}

void time_sync_notification_cb(struct timeval *tv){
    ESP_LOGI(TAG, "Notification of a time synchronization event");
}


static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
        case SYSTEM_EVENT_STA_START:
            printf("Wifi STA initialized\n");
//            wifiClient->onInitialized();
//            wifiClient->connect();
            break;
        case SYSTEM_EVENT_STA_GOT_IP:
            printf("Wifi STA have ip\n");
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
            wifiClient->onConnected();
            break;
        case SYSTEM_EVENT_STA_DISCONNECTED:
            printf("Wifi STA disconnected\n");
            ESP_LOGI(TAG, "disconnect reason: %d", event->event_info.disconnected.reason);
            wifiClient->onDisconnected();
            wifiClient->connect();
            xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
            break;
        case SYSTEM_EVENT_AP_START:
            printf("AP started\n");
            wifiServer->onStartedServingCallback();

        case SYSTEM_EVENT_AP_STACONNECTED:
            ESP_LOGI(TAG, "station:"MACSTR" join, AID=%d",
                     MAC2STR(event->event_info.sta_connected.mac),
                     event->event_info.sta_connected.aid);
            wifiServer->onClientConnectedCallback();
            break;
        case SYSTEM_EVENT_AP_STADISCONNECTED:
            ESP_LOGI(TAG, "station:"MACSTR"leave, AID=%d",
                     MAC2STR(event->event_info.sta_disconnected.mac),
                     event->event_info.sta_disconnected.aid);
            wifiServer->onClientDisconnectedCallback();
            break;
        default:
            break;
    }
    return ESP_OK;
}

//Function that prints the reason by which ESP32 has been awaken from sleep
void print_wakeup_reason(){
    esp_sleep_wakeup_cause_t wakeup_reason;
    wakeup_reason = esp_sleep_get_wakeup_cause();
    switch(wakeup_reason)
    {
        case ESP_SLEEP_WAKEUP_EXT0 : printf("Wakeup caused by external signal using RTC_IO\n"); break;
        case ESP_SLEEP_WAKEUP_EXT1 : printf("Wakeup caused by external signal using RTC_CNTL\n"); break;
        case ESP_SLEEP_WAKEUP_TIMER : printf("Wakeup caused by timer\n"); break;
        case ESP_SLEEP_WAKEUP_TOUCHPAD : printf("Wakeup caused by touchpad\n"); break;
        case ESP_SLEEP_WAKEUP_ULP : printf("Wakeup caused by ULP program\n"); break;
        default : printf("Wakeup was not caused by deep sleep\n"); break;
    }
}

extern "C" {
    void app_main() {
        esp_timer_init();
        gpio_config_t o_conf;
        //disable interrupt
        o_conf.intr_type = (gpio_int_type_t)GPIO_PIN_INTR_DISABLE;
        //set as output mode
        o_conf.mode = GPIO_MODE_OUTPUT;
        //bit mask of the pins that you want to set,e.g.GPIO18/19
        o_conf.pin_bit_mask = static_cast<uint64_t>(GPIO_OUTPUT_PIN_SEL);
        //disable pull-down mode
        o_conf.pull_down_en = (gpio_pulldown_t)0;
        //disable pull-up mode
        o_conf.pull_up_en = (gpio_pullup_t)0;
        //configure GPIO with the given settings
        gpio_config(&o_conf);

        gpio_set_level(GPIO_LED, 1);

        gpio_config_t i_conf;
        //interrupt of rising edge
        i_conf.intr_type = (gpio_int_type_t)GPIO_PIN_INTR_DISABLE;
        i_conf.pin_bit_mask = 1ULL<<GPIO_BUTTON;
        //set as input mode
        i_conf.mode = GPIO_MODE_INPUT;
        //enable pull-up mode
        i_conf.pull_up_en = gpio_pullup_t::GPIO_PULLUP_ENABLE;
        gpio_config(&i_conf);

        esp_sleep_enable_ext0_wakeup(GPIO_BUTTON,0);

        initialize_nvs();

        adc2_config_channel_atten(ADC2_CHANNEL_8, ADC_ATTEN_11db);
        ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL));
        wifi_event_group = xEventGroupCreate();

        MessageTimer.enable(true);
        MessageTimer.start(true);

        httpClient.setReceiveBuffer(buffer);

        i2c.open();
        accelerometer.init();
        sht21.init();
        registry.init();

        sys.init();

        sntp_setoperatingmode(SNTP_OPMODE_POLL);
        sntp_setservername(0, "pool.ntp.org");

        print_wakeup_reason();
        printf("Entering app\n");
        bool button_state = true;
        bool led_state = false;
        for(int i=0;i<30;i++) {
            button_state = !gpio_get_level((gpio_num_t)GPIO_BUTTON);
            if(!button_state){
                break;
            }
            std::this_thread::sleep_for(100ms);
            gpio_set_level(GPIO_LED,led_state);
            led_state = !led_state;
        }
        gpio_set_level(GPIO_LED, 1);

        bob.run(button_state);

        while(1);

    }
}